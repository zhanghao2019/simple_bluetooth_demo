package com.example.bluetoothdemo;

import android.bluetooth.BluetoothDevice;

public class Device {
    public BluetoothDevice device;
    public int rssi;

    Device(BluetoothDevice device,int rssi){
        this.device=device;
        this.rssi =rssi;
    }

}
